#+TITLE: dtos-pkgbuild
#+DESCRIPTION: A repository of PKGBUILDs for dtos-core-repo.
#+AUTHOR: Derek Taylor (DistroTube)

* dtos-pkgbuild
A repository of PKGBUILDs for programs in the [[https://gitlab.com/dwt1/dtos-core-repo][DTOS core repo]].
